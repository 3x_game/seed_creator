# Readme seed_creator

* Crate can be used to add into a game created for [wasm4](https://wasm4.org) fantasy console
  * As far as I know, wasm4 provides no API to create a seed value or random numbers
  * Some games use random number generation, e.g. for random level generation
* But it does not depend directly on any APIs of wasm4 but it follows its specification
  * This means it could be modified and used in other fantasy consoles or other set-ups
* Usage can be found in the crate and API documentation of this crate
  * Can be created with `cargo doc`
* Code Coverage can be calculated with [cargo-llvm-cov](https://lib.rs/crates/cargo-llvm-cov)
* Find generated code coverage report below
* License: MIT

## Code coverage report

* Date 2022-10-08

```text
Filename                      Regions    Missed Regions     Cover   Functions  Missed Functions  Executed       Lines      Missed Lines     Cover    Branches   Missed Branches     Cover
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
button.rs                          11                 1    90.91%           4                 0   100.00%          39                 0   100.00%           0                 0         -
color.rs                            2                 1    50.00%           2                 1    50.00%           2                 1    50.00%           0                 0         -
grid.rs                            59                 1    98.31%           8                 0   100.00%         125                 1    99.20%           0                 0         -
lib.rs                              8                 0   100.00%           3                 0   100.00%          66                 0   100.00%           0                 0         -
rect.rs                             2                 1    50.00%           2                 1    50.00%           2                 1    50.00%           0                 0         -
seed_creator.rs                    23                 0   100.00%           9                 0   100.00%          84                 0   100.00%           0                 0         -
text.rs                             1                 0   100.00%           1                 0   100.00%           3                 0   100.00%           0                 0         -
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TOTAL                             106                 4    96.23%          29                 2    93.10%         321                 3    99.07%           0                 0         -
```
