use crate::color::Color;
use crate::rect::Rect;
use crate::render::Render;
use crate::text::Text;
use crate::update::Update;

pub(crate) struct Button {
    x: i32,
    y: i32,
    width: u16,
    height: u16,
    pressed: bool,
}

pub(crate) struct RenderList<'a> {
    pub(crate) rect: Rect,
    pub(crate) text: Text<'a>,
}

impl Button {
    pub(crate) const fn from(x: i32, y: i32, width: u16, height: u16) -> Button {
        Button {
            x,
            y,
            width,
            height,
            pressed: false,
        }
    }
    pub(crate) fn pressed(&self) -> bool {
        self.pressed
    }
}

impl Update for Button {
    fn update(&mut self, pointer: (u16, u16)) {
        let (x, y) = pointer;
        let x = i32::from(x);
        let y = i32::from(y);
        if x >= self.x
            && x < self.x + i32::from(self.width)
            && y >= self.y
            && y < self.y + i32::from(self.height)
        {
            self.pressed = true;
        }
    }
}

impl<'a> Render<RenderList<'a>> for Button {
    fn render(&self) -> RenderList<'a> {
        let rect = Rect {
            x: self.x,
            y: self.y,
            width: self.width.into(),
            height: self.height.into(),
            color: Color::Transparent,
            stroke_color: Color::Color1,
        };
        let text = Text {
            x: self.x + 2,
            y: self.y + 2,
            text: "Accept",
            color: Color::Color1,
        };
        RenderList { rect, text }
    }
}
