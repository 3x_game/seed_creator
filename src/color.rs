/// Colors used for elements to be rendered.
///
/// ## Background
///
/// Derived from the 4-color (incl. transparency) of the retro gaming console [wasm4](https://wasm4.org/).
/// However these colors are implemented independent of wasm4 but it represents the same ideas.
#[derive(Copy, Clone, Default)]
#[repr(u8)]
pub enum Color {
    /// Color is full transparent.
    Transparent = 0,
    /// Color is the background color. Concept derived from wasm4.
    BackgroundColor = 1,
    /// Color 1: not transparent and not the background color. Is also the default color.
    #[default]
    Color1 = 2,
    /// Color 2: not transparent and not the background color and not Color1.
    Color2 = 3,
    /// Color 3: not transparent and not the background color and not Color1 or Color2.
    Color3 = 4,
}
