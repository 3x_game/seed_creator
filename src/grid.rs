use crate::color::Color;
use crate::rect::Rect;
use crate::render::Render;
use crate::update::Update;

const ROWS: u8 = 5;
const COLS: u8 = 6;
const CELL_LENGTH: u8 = 20;

pub(crate) struct Grid {
    value: u32,
    x: u16,
    y: u16,
}

impl Grid {
    pub(crate) const fn at_position(x: u16, y: u16) -> Grid {
        Grid { x, y, value: 0 }
    }
    pub(crate) const fn seed(&self) -> u32 {
        self.value
    }
}

impl Update for Grid {
    fn update(&mut self, pointer: (u16, u16)) {
        let x = pointer.0;
        let y = pointer.1;
        if x >= self.x
            && x < self.x + u16::from(COLS * CELL_LENGTH)
            && y >= self.y
            && y < self.y + u16::from(ROWS * CELL_LENGTH)
        {
            let col = (x - self.x) / u16::from(CELL_LENGTH);
            let row = (y - self.y) / u16::from(CELL_LENGTH);

            let shift_value = col + row * u16::from(COLS);
            let update_value = 1 << shift_value;
            self.value ^= update_value;
        }
    }
}

impl Render<[Rect; 30]> for Grid {
    fn render(&self) -> [Rect; 30] {
        let mut render_list = [Rect::default(); 30];
        let mut v = self.value;
        let width: u32 = CELL_LENGTH.into();
        let height: u32 = CELL_LENGTH.into();
        for r in 0..ROWS {
            for c in 0..COLS {
                let color = if v & 1 == 1 {
                    Color::Color3
                } else {
                    Color::Color2
                };
                v >>= 1;
                let index: usize = usize::from(c + COLS * r);
                let x: i32 = i32::from(self.x + u16::from(c * CELL_LENGTH));
                let y: i32 = i32::from(self.y + u16::from(r * CELL_LENGTH));
                render_list[index] = Rect {
                    x,
                    y,
                    width,
                    height,
                    color,
                    stroke_color: Color::Color1,
                };
            }
        }

        render_list
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_render() {
        let mut grid = Grid::at_position(0, 0);
        grid.value = 0b0010_1010_1010_1010_1010_1010_1010_1010;

        let rect_list = grid.render();

        let mut color = Color::Color2;
        let mut x = 0;
        let mut y = 0;
        let mut c = 0;
        let mut r = 0;

        for rect in rect_list {
            assert_eq!(rect.color as u8, color as u8);
            color = match color {
                Color::Color2 => Color::Color3,
                Color::Color3 => Color::Color2,
                _ => Color::BackgroundColor,
            };
            assert_eq!(rect.x, x);
            assert_eq!(rect.y, y);
            c += 1;
            if c == i32::from(COLS) {
                c = 0;
                r += 1;
            }
            x = c * i32::from(CELL_LENGTH);
            y = r * i32::from(CELL_LENGTH);
        }
    }

    #[test]
    fn test_update() {
        let mut grid = Grid {
            value: 0,
            x: 20,
            y: 20,
        };

        grid.update((0, 0));
        assert_eq!(grid.value, 0);

        grid.update((20, 20));
        assert_eq!(grid.value, 1);

        grid.update((121, 101));
        assert_eq!(grid.value, (1 << 29) ^ 1);

        grid.update((20, 20));
        assert_eq!(grid.value, 1 << 29);

        grid.update((120, 100));
        assert_eq!(grid.value, 0);

        grid.update((52, 32));
        assert_eq!(grid.value, 2);

        grid.update((79, 39));
        assert_eq!(grid.value, 6);

        grid.update((73, 28));
        assert_eq!(grid.value, 2);

        grid.update((40, 20));
        assert_eq!(grid.value, 0);

        grid.update((93, 29));
        assert_eq!(grid.value, 8);

        grid.update((84, 23));
        assert_eq!(grid.value, 0);

        grid.update((51, 47));
        assert_eq!(grid.value, 128);

        grid.update((59, 59));
        assert_eq!(grid.value, 0);

        grid.update((106, 71));
        assert_eq!(grid.value, 65536);

        grid.update((101, 60));
        assert_eq!(grid.value, 0);

        grid.update((40, 100));
        assert_eq!(grid.value, 1 << 25);

        grid.update((59, 100));
        assert_eq!(grid.value, 0);

        grid.update((19, 30));
        assert_eq!(grid.value, 0);

        grid.update((88, 19));
        assert_eq!(grid.value, 0);

        grid.update((140, 68));
        assert_eq!(grid.value, 0);

        grid.update((109, 120));
        assert_eq!(grid.value, 0);
    }
}
