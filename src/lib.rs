//! Crate that can be used for creating a seed.
//!
//! The crate is inspired by the [wasm4](https://wasm4.org/) fantasy retro console.
//! This console (to my knowledge) do not provide an API to get a seed value.
//! But if a game needs to generate random items, a seed to initialize a random number
//! generator is super useful.
//!
//! ## Example
//!
//! ```rust
//! // First the traits and required structs must be imported via use
//! use seed_creator::{SeedCreator,Update,Render,Rect,Text,Color,RenderList};
//!
//! // Then the SeedCreator needs to be created.
//! // In a game loop setting as in [wasm4](https://wasm4.org) the SeedCreator
//! // needs to be static mut. This is left out for simplicity reasons in this example.
//! let mut seed_creator = SeedCreator::new();
//!
//! // When the player interacts with the game, pointer events will be created, e.g. a left
//! // mouse click or a touch. It is assumed that pointer events are supported.
//! // Use the coordinates to update the seed_creator. In the below example the first cell is clicked.
//! seed_creator.update((30,40));
//!
//! // Usually the seed should only be used when the player accepted the pattern.
//! // But there may be use cases to display the seed or something else during the
//! // pattern creation.
//! assert_eq!(1, seed_creator.seed());
//!
//! // As long as the player did not press the "Accept" button, the SeedCreator renders.
//! // I.e. upon calling render it returns an Option containing the RenderList with elements
//! // that can then be used to draw these elements on the screen.
//! let option_render_list = seed_creator.render();
//! assert!(option_render_list.is_some());
//! let render_list = option_render_list.unwrap();
//! // the first rectangular in the render list represent the cell the player
//! // clicked in this example. Its color should be Color3.
//! assert_eq!(Color::Color3 as u8, render_list.rects[0].color as u8);
//!
//! // When the player clicks the "Accept" button, e.g. by clicking on (130,150):
//! seed_creator.update((130,150));
//! // Now the SeedCreator is in the "accepted state": The seed is finalized.
//! assert!(seed_creator.accepted());
//! // The SeedCreator then no longer renders, i.e. returns None instead.
//! let none_render_list = seed_creator.render();
//! assert!(none_render_list.is_none());
//! ```
#![no_std]
#![warn(missing_docs)]
mod button;
mod color;
mod grid;
mod rect;
mod render;
mod seed_creator;
mod text;
mod update;

pub use self::seed_creator::RenderList;
pub use self::seed_creator::SeedCreator;
pub use color::Color;
pub use rect::Rect;
pub use render::Render;
pub use text::Text;
pub use update::Update;
