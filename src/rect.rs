use crate::color::Color;

/// A struct representing a rectangular used for render output.
#[derive(Default, Copy, Clone)]
pub struct Rect {
    /// The x-coordinate of the rectangular.
    pub x: i32,
    /// The y-coordinate of the rectangular.
    pub y: i32,
    /// The width of the rectangular.
    pub width: u32,
    /// The height of the rectangular.
    pub height: u32,
    /// The (fill-)color of the rectangular.
    pub color: Color,
    /// The stroke color of the rectangular.
    pub stroke_color: Color,
}
