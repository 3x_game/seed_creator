/// Render trait used for the components of this crate.
///
/// The components return elements that can then be used to render the components on the screen
/// with the dedicated graphical API.
pub trait Render<T> {
    /// Returns the graphical elements of a component.
    fn render(&self) -> T;
}
