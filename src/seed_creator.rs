use crate::button::Button;
use crate::color::Color;
use crate::grid::Grid;
use crate::rect::Rect;
use crate::render::Render;
use crate::text::Text;
use crate::update::Update;

const HEADLINE: &str = "Create random seed:";
const EXPLANATION_LINE_1: &str = "Click on cells to";
const EXPLANATION_LINE_2: &str = "create pattern.";

/// SeedCreator struct.
///
/// Defines the components and methods required to use a seed creator.
/// When calling [crate::Update::update] it returns an [Option] either containing [RenderList] or [None].
/// This is based on the fact if the "Accepted" button is pressed. If the button was not pressed it renders
/// the explanatory texts, the rectangles that represent the grid and the outline of the button.
pub struct SeedCreator {
    grid: Grid,
    button: Button,
}

impl SeedCreator {
    /// Create a new seed_creator.
    ///
    /// It is currently implemented by respecting the specifications of the [wasm4](https://wasm4.org/) retro fantasy console.
    /// This means:
    /// * It is placed in a 160x160 pixel screen
    /// * Its "updatable" components (a grid and an accept button) are placed with respect to the 160x160 pixel screen
    /// * This subsequently means that "clicking" on any pixel may change the state of the SeedCreator.
    /// ## Example
    ///
    /// ```rust
    /// use seed_creator::{SeedCreator,Update};
    /// let mut seed_creator = SeedCreator::new();
    ///
    /// // then one can update the seed_creator based on the coordinates
    /// // of a pointer event.
    /// seed_creator.update((0,0));
    ///
    /// // (0,0) does not click on a component that is updated, therefore the following
    /// // assertions are true.
    /// assert_eq!(0,seed_creator.seed());
    /// assert!(!seed_creator.accepted());
    /// ```
    pub const fn new() -> Self {
        SeedCreator {
            grid: Grid::at_position(20, 30),
            button: Button::from(108, 148, 52, 12),
        }
    }
    /// Returns the current seed value of the seed creator.
    ///
    /// This value is defined by clicking on the grid displayed on the screen.
    /// ```rust
    /// use seed_creator::{SeedCreator,Update};
    /// let mut seed_creator = SeedCreator::new();
    ///
    /// seed_creator.update((30,40));
    ///
    /// // (30,40) hits the first cell of the grid.
    /// // This cell corresponds with the least significant bit of the seed value.
    /// assert_eq!(1,seed_creator.seed());
    /// ```
    pub const fn seed(&self) -> u32 {
        self.grid.seed()
    }
    /// Returns true is a pointer event hit the "Accept" button.
    ///
    /// If this has happened, then the SeedCreator does not render anything anymore.
    /// Reason is to indicate that the seed creation is now finished and cannot be changed anymore.
    ///
    /// ```rust
    /// use seed_creator::{SeedCreator,Update};
    /// let mut seed_creator = SeedCreator::new();
    ///
    /// seed_creator.update((130,150));
    ///
    /// // When the "Accept" button is hit, then the SeedCreator
    /// // returns true when accepted is called.
    /// assert!(seed_creator.accepted());
    /// ```
    pub fn accepted(&self) -> bool {
        self.button.pressed()
    }
}

/// Represents the elements that are created when [crate::Update::update] is called on the [SeedCreator].
pub struct RenderList<'a> {
    /// All [crate::Text]s representing the grid and the button of the [SeedCreator].
    pub texts: [Text<'a>; 4],
    /// All [crate::Rect]s representing the grid and the button of the [SeedCreator].
    pub rects: [Rect; 31],
}

impl<'a> Render<Option<RenderList<'a>>> for SeedCreator {
    fn render(&self) -> Option<RenderList<'a>> {
        let mut render_list = None;
        if !self.button.pressed() {
            let button_render_list = self.button.render();
            let mut rects: [Rect; 31] = Default::default();
            for (i, r) in self.grid.render().into_iter().enumerate() {
                rects[i] = r;
            }
            rects[30] = button_render_list.rect;
            let texts = [
                Text::from(1, 1, HEADLINE, Color::Color1),
                Text::from(1, 10, EXPLANATION_LINE_1, Color::Color1),
                Text::from(1, 19, EXPLANATION_LINE_2, Color::Color1),
                button_render_list.text,
            ];

            render_list = Some(RenderList { texts, rects });
        }
        render_list
    }
}

impl Update for SeedCreator {
    fn update(&mut self, pointer: (u16, u16)) {
        self.button.update(pointer);
        if !self.button.pressed() {
            self.grid.update(pointer);
        }
    }
}
