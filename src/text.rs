use crate::color::Color;

/// Struct representing a text to rendered on the screen.
pub struct Text<'a> {
    /// The x-coordinate of the text.
    pub x: i32,
    /// The y-coordinate of the text.
    pub y: i32,
    /// The actual content of the text.
    pub text: &'a str,
    /// The color of the text.
    pub color: Color,
}

impl<'a> Text<'a> {
    pub(crate) fn from(x: i32, y: i32, text: &'a str, color: Color) -> Text {
        Text { x, y, text, color }
    }
}
