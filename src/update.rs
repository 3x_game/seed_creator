/// Update trait used by the components of these crate.
///
/// Components are updated based on the position of a pointer event (e.g. a mouse click).
pub trait Update {
    /// Update the component with the coordinates of a pointer event.
    fn update(&mut self, pointer: (u16, u16));
}
